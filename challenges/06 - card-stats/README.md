# Task 6 - Card Statistics

This question refers to the GetCardFromDeck method of the Breakthrough class and the creation of a new method, DisplayStats, modifying two existing methods, AddCard and RemoveCard, as well as adding three new attributes, NumPicks, NumFiles and NumKeys, in the CardCollection class.

Introduce a stats / card count to the CardCollection class which keeps track of which cards have come out of the deck and calculates the % chance that the next card tile in the deck is X type (or XYZ types).

Introduce three new attributes to the CardCollection class called NumPicks, NumFiles and NumKeys, which will be updated every time a ToolCard is added to or removed from the CardCollection.

Create a new method in the CardCollection class called DisplayStats. This method should calculate the percentage chance of the next card being a key, pick or file based on the number of each card and the number of cards left in the deck.

When the player receives a difficulty card, use the DisplayStats method together with the GetNumberOfCards method in the CardCollection class to display the following on the screen before they choose ‘lose a key or discard 5 cards from the deck’.

_There is a X% chance that the next card will be a key, a Y% chance that it will be a file and a Z% chance that it will be a pick._

The percentages should be displayed to two decimal places. Replace X, Y and Z with the appropriate values. Note that they will not normally add up to 100% because there are also difficulty cards in the deck.
